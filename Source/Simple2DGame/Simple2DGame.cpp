// Fill out your copyright notice in the Description page of Project Settings.

#include "Simple2DGame.h"
#include "Modules/ModuleManager.h"

IMPLEMENT_PRIMARY_GAME_MODULE( FDefaultGameModuleImpl, Simple2DGame, "Simple2DGame" );

DEFINE_LOG_CATEGORY(S2GLogs);