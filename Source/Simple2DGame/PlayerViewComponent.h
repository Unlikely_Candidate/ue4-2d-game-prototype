// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "Engine.h"
#include "PaperFlipbookComponent.h"
#include "PaperFlipbook.h"
#include "PlayerViewComponent.generated.h"

/**
 * 
 */
UCLASS()
class SIMPLE2DGAME_API UPlayerViewComponent : public UPaperFlipbookComponent
{
	GENERATED_BODY()

public:
	UPlayerViewComponent();

	virtual void BeginPlay() override;
	virtual void EndPlay(const EEndPlayReason::Type Reason) override;

	void Update(const FVector& Velocity);

protected:
	UPROPERTY(EditAnywhere, Category = Assets)
	TAssetPtr<UPaperFlipbook> IdleAnim;

	UPROPERTY(EditAnywhere, Category = Assets)
	TAssetPtr<UPaperFlipbook> RunAnim;

	void LoadAssets();
	void FreeAssets();

private:
	float LastFacingDir;
};
