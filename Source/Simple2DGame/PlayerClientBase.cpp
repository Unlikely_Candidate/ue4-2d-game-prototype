// Fill out your copyright notice in the Description page of Project Settings.

#include "PlayerClientBase.h"
#include "PaperSpriteComponent.h"
#include "PlayerMovementComponent.h"
#include "PlayerViewComponent.h"
#include "Net/UnrealNetwork.h"

// Sets default values
APlayerClientBase::APlayerClientBase()
	: LastRepMovement(0.0f, 0.0f)
	, CurrentMovement(0.0f, 0.0f)
{
 	// Set this pawn to call Tick() every frame.  You can turn this off to improve performance if you don't need it.
	PrimaryActorTick.bCanEverTick = true;

	Collision = CreateDefaultSubobject<UBoxComponent>(TEXT("Collision"));

	RootComponent = Collision;

	View = CreateDefaultSubobject<UPlayerViewComponent>(TEXT("View"));
	View->AttachToComponent(RootComponent, FAttachmentTransformRules::KeepRelativeTransform);
	
	MovementController = CreateDefaultSubobject<UPlayerMovementComponent>(TEXT("MovementController"));
	MovementController->UpdatedComponent = RootComponent;

	SpringArm = CreateDefaultSubobject<USpringArmComponent>(TEXT("SpringArm"));
	SpringArm->bEnableCameraLag = true;
	SpringArm->bEnableCameraRotationLag = false;
	SpringArm->bDoCollisionTest = false;
	SpringArm->bUsePawnControlRotation = false;
	SpringArm->TargetArmLength = 500.0f;
	SpringArm->CameraLagSpeed = 2.0f;
	SpringArm->AttachToComponent(RootComponent, FAttachmentTransformRules::KeepRelativeTransform);
	SpringArm->SetWorldRotation(FRotator(0.0f, 0.0f, 0.0f));

	Camera = CreateDefaultSubobject<UCameraComponent>(TEXT("Camera"));
	Camera->bUsePawnControlRotation = false;
	Camera->ProjectionMode = ECameraProjectionMode::Orthographic;
	Camera->SetOrthoWidth(1024.0f);
	Camera->SetAspectRatio(3.0f / 4.0f);
	Camera->AttachToComponent(SpringArm, FAttachmentTransformRules::KeepRelativeTransform, USpringArmComponent::SocketName);

	bReplicates = true;
	bAlwaysRelevant = true;
}

// Called when the game starts or when spawned
void APlayerClientBase::BeginPlay()
{
	Super::BeginPlay();
}

// Called every frame
void APlayerClientBase::Tick(float DeltaTime)
{
	Super::Tick(DeltaTime);
	
	View->Update(MovementController->GetLastInputVector());

	// if we're on the client that controlls this Pawn
	if (Controller && Controller->IsLocalPlayerController())
	{
		if (CurrentMovement != LastRepMovement)
		{
			LastRepMovement = CurrentMovement;
			if (GetNetMode() == ENetMode::NM_Client)
			{
				ServerMove(LastRepMovement, GetActorLocation());
			}
			else
			{
				ClientMove(LastRepMovement, GetActorLocation());
			}
		}
	}

	AddMovementInput(FVector(CurrentMovement.X, 0.0f, CurrentMovement.Y));
}

// Called to bind functionality to input
void APlayerClientBase::SetupPlayerInputComponent(UInputComponent* PlayerInputComponent)
{
	check(PlayerInputComponent);

	Super::SetupPlayerInputComponent(PlayerInputComponent);

	PlayerInputComponent->BindAxis("MoveX", this, &APlayerClientBase::MoveX);
	PlayerInputComponent->BindAxis("MoveY", this, &APlayerClientBase::MoveY);
}

void APlayerClientBase::MoveX(float AxisValue)
{
	CurrentMovement.X = AxisValue;
}

void APlayerClientBase::MoveY(float AxisValue)
{
	CurrentMovement.Y = AxisValue;
}

void APlayerClientBase::ServerMove_Implementation(FVector2D Velocity, FVector Location)
{
	if (GetNetMode() != ENetMode::NM_Client)
	{
		if (LastRepMovement == CurrentMovement)
		{
			ClientMove(Velocity, Location);
			LastRepMovement = Velocity;
		}

		CurrentMovement = Velocity;
		SetActorLocation(Location);
	}
}

bool APlayerClientBase::ServerMove_Validate(FVector2D Velocity, FVector Location)
{
	return true;
}

void APlayerClientBase::ClientMove_Implementation(FVector2D Velocity, FVector Location)
{
	if (GetNetMode() == ENetMode::NM_Client)
	{
		if (!Controller || !Controller->IsLocalPlayerController())
		{
			LastRepMovement = Velocity;
			CurrentMovement = Velocity;
			SetActorLocation(Location);
		}
	}
}

bool APlayerClientBase::ClientMove_Validate(FVector2D Velocity, FVector Location)
{
	return true;
}
