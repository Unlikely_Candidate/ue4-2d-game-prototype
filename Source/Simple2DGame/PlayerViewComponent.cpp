// Fill out your copyright notice in the Description page of Project Settings.

#include "PlayerViewComponent.h"

#include "PlayerClientBase.h"
#include "PlayerMovementComponent.h"

UPlayerViewComponent::UPlayerViewComponent()
	: LastFacingDir(0.0f)
{
}

void UPlayerViewComponent::BeginPlay()
{
	Super::BeginPlay();
	LoadAssets();
}

void UPlayerViewComponent::EndPlay(const EEndPlayReason::Type Reason)
{
	FreeAssets();
	Super::EndPlay(Reason);
}

void UPlayerViewComponent::Update(const FVector& Velocity)
{
	FVector Dir;
	float Length;
	Velocity.ToDirectionAndLength(Dir, Length);

	if (Dir.X != 0.0)
	{
		LastFacingDir = Dir.X;
	}

	if (FMath::IsNearlyEqual(Length, 0.0f))
	{
		SetFlipbook(IdleAnim.Get());
	}
	else
	{
		SetFlipbook(RunAnim.Get());
	}

	SetRelativeRotation(FRotator(0.0f, LastFacingDir > 0.0 ? 0.0 : -180.0f, 0.0));
}

void UPlayerViewComponent::LoadAssets()
{
	FStreamableManager Streamable;

	if (IdleAnim.IsPending())
	{
		const FStringAssetReference& AssetRef = IdleAnim.ToStringReference();
		IdleAnim = Cast<UPaperFlipbook>(Streamable.LoadSynchronous(AssetRef));
	}

	if (RunAnim.IsPending())
	{
		const FStringAssetReference& AssetRef = RunAnim.ToStringReference();
		RunAnim = Cast<UPaperFlipbook>(Streamable.LoadSynchronous(AssetRef));
	}
}

void UPlayerViewComponent::FreeAssets()
{
	FStreamableManager Streamable;

	if (IdleAnim.IsValid())
	{
		const FStringAssetReference& AssetRef = IdleAnim.ToStringReference();
		Streamable.Unload(AssetRef);
	}

	if (RunAnim.IsValid())
	{
		const FStringAssetReference& AssetRef = RunAnim.ToStringReference();
		Streamable.Unload(AssetRef);
	}
}
