// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "Simple2DGame.h"
#include "GameFramework/Pawn.h"
#include "PlayerClientBase.generated.h"


UCLASS()
class SIMPLE2DGAME_API APlayerClientBase : public APawn
{
	GENERATED_BODY()

public:
	// Sets default values for this pawn's properties
	APlayerClientBase();

	// Called every frame
	virtual void Tick(float DeltaTime) override;

	// Called to bind functionality to input
	virtual void SetupPlayerInputComponent(class UInputComponent* PlayerInputComponent) override;

protected:
	UPROPERTY(VisibleAnywhere, BlueprintReadOnly, Category = "Character")
	class USpringArmComponent* SpringArm;

	UPROPERTY(VisibleAnywhere, BlueprintReadOnly, Category = "Character")
	class UCameraComponent* Camera;
	
	UPROPERTY(VisibleAnywhere, BlueprintReadOnly, Category = "Character")
	class UPlayerMovementComponent* MovementController;

	UPROPERTY(VisibleAnywhere, BlueprintReadOnly, Category = "Character")
	class UPlayerViewComponent* View;

	UPROPERTY(VisibleAnywhere, BlueprintReadOnly, Category = "Character")
	class UBoxComponent* Collision;

	// Called when the game starts or when spawned
	virtual void BeginPlay() override;

	void MoveX(float AxisValue);
	void MoveY(float AxisValue);

	UFUNCTION(Server, Reliable, WithValidation)
	void ServerMove(FVector2D Velocity, FVector Location);

	UFUNCTION(NetMulticast, Reliable, WithValidation)
	void ClientMove(FVector2D Velocity, FVector Location);

	UPROPERTY(Transient)
	FVector2D CurrentMovement;

	UPROPERTY(Transient)
	FVector2D LastRepMovement;
};
