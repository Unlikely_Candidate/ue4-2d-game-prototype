// Fill out your copyright notice in the Description page of Project Settings.

#include "PlayerMovementComponent.h"

UPlayerMovementComponent::UPlayerMovementComponent()
{
	SpeedUnrealUnitsPerSec = 600.0f;
}

void UPlayerMovementComponent::TickComponent(float DeltaTime, enum ELevelTick TickType, FActorComponentTickFunction* ThisTickFunction)
{
	Super::TickComponent(DeltaTime, TickType, ThisTickFunction);

	if (!PawnOwner || !UpdatedComponent || ShouldSkipUpdate(DeltaTime))
	{
		return;
	}

	FVector DesiredMovementThisFrame = ConsumeInputVector().GetClampedToMaxSize(1.0f) * DeltaTime * SpeedUnrealUnitsPerSec;

	if (DesiredMovementThisFrame.IsNearlyZero())
	{
		return;
	}

	FHitResult Hit;
	SafeMoveUpdatedComponent(DesiredMovementThisFrame, UpdatedComponent->GetComponentRotation(), true, Hit);

	if (Hit.IsValidBlockingHit())
	{
		SlideAlongSurface(DesiredMovementThisFrame, 1.0f - Hit.Time, Hit.Normal, Hit);
	}
}

void UPlayerMovementComponent::SetSpeed(float UnrealUnitsPerSec)
{
	SpeedUnrealUnitsPerSec = UnrealUnitsPerSec;
}
